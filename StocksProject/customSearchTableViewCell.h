//
//  customSearchTableViewCell.h
//  StocksProject
//
//  Created by Rennel Sangria on 8/11/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customSearchTableViewCell : UITableViewCell
@property (nonatomic, retain) UILabel* stockSymbolLbl;
@property (nonatomic, retain) UILabel* exchangeLbl;
@property (nonatomic, retain) UILabel* stockNameLbl;


@end
