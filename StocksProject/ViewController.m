//
//  ViewController.m
//  StocksProject
//
//  Created by Rennel Sangria on 8/3/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end


@implementation ViewController

static NSString *yahooLoadStockDetailsURLString = @"http://query.yahooapis.com/v1/public/yql?q=select%%20*%%20from%%20yahoo.finance.quotes%%20where%%20symbol%%20%%3D%%20%%22%@%%22&format=json&env=store%%3A%%2F%%2Fdatatables.org%%2Falltableswithkeys&callback=cbfunc";



- (id) initWithStyle:(UITableViewStyle)style{
    self = [super initWithStyle:style];
    if (self){
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = NO;
    }
    return self;
}


- (PFQuery *)queryForTable{
    PFQuery *query = [PFQuery queryWithClassName: @"Stock"];
    if ([self.objects count] == 0){
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByAscending:@"createdAt"];
    return query;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSLog(@"number of objects %@",self.objects count);
    return [self.objects count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject*)object{
    
    //use the custom table view cell
    CustomTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    //display the stock name
    
    cell.stockLbl.text = [object objectForKey:@"stockSymbol"];
    if (cell.stockLbl.text == nil){
        NSLog(@"no stocks found");
    }
    //display the stock current value
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        
        NSDictionary* array = [self detailsOfStockSymbol:cell.stockLbl.text];
        
        dispatch_async(dispatch_get_main_queue(), ^{ // 2
            //Update my UI
            float numShares = [[object objectForKey:@"numberOfShares"]floatValue];
            float currentValue = [[[[[array objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"quote"] objectForKey:@"Ask"]floatValue];
            //NSLog(@"The current ask value is %f", currentValue);
            float sharesTimesCurrentValue = numShares * currentValue;
            //NSString *currentTotalValueStr = [NSNumberFormatter localizedStringFromNumber:sharesTimesCurrentValue  numberStyle:NSNumberFormatterCurrencyStyle];
            
            cell.valueLbl.text= [NSString stringWithFormat: @"$%.2f", sharesTimesCurrentValue];
            
            float initialPrice = [[object objectForKey:@"initialPrice"]floatValue];
            float initPriceTimesShares = numShares * initialPrice;
            float totalFees = [[object objectForKey:@"fees"]floatValue];
            float originalBuyIn = initPriceTimesShares + totalFees;
            float profit = sharesTimesCurrentValue - originalBuyIn;
            
            //convert float to currency
            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
            NSString *profitNumberAsString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:profit]];
            cell.gainLossLbl.text = profitNumberAsString;
            //cell.gainLossLbl.text = [NSString stringWithFormat: @"$%.2f", profit];
            
            //change color of the background; green for +, red for (-)
            if(profit  < 0.0){
                cell.gainLossLbl.backgroundColor = [UIColor redColor];
               // cell.gainLossLbl.text = [NSString stringWithFormat: @"-$%.2f", profit];
            }
            
            else if(profit > 0.0){
                // NSLog(@"%f", [num1 doubleValue]);
                cell.gainLossLbl.backgroundColor =  [UIColor colorWithRed:106/255.0f green:183/255.0f blue:9/255.0f alpha:1.0f];
                //NSString *plus = @"+";
                cell.gainLossLbl.text = [NSString stringWithFormat: @"+$%.2f", profit];
            }
            else{
                cell.gainLossLbl.backgroundColor = [UIColor whiteColor];
                cell.gainLossLbl.textColor = [UIColor blackColor];
                
            }

        });
    });
    
    return cell;
}

// method for tableView didSelectRowAtIndexPath
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    CALayer *separator = [CALayer layer];
//    separator.backgroundColor = [UIColor lightGrayColor].CGColor;
//    separator.frame = CGRectMake(0, 100, self.view.frame.size.width, 1);
//   // [cell.layer addSublayer:separator];
//    [self.view.layer addSublayer:separator];
    
    StockDetailsViewController* sdvc = [StockDetailsViewController new];
    sdvc.theStockObject = [self.objects objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:sdvc animated:YES];
    NSLog(@"The object passed via  view controller: %@", sdvc.theStockObject);


}





- (void)viewDidLoad {
    [super viewDidLoad];
    
   // NSLog(@"screens = %@", [UIScreen screens]);
    //NSLog(@"self.frame= %@", NSStringFromCGRect([[UIScreen mainScreen] bounds]));
   // NSLog(@"self.view.frame= %@", NSStringFromCGRect(self.view.frame));
    
//displays all the available fonts
//    for (NSString* family in [UIFont familyNames])
//    {
//        NSLog(@"%@", family);
//        
//        for (NSString* name in [UIFont fontNamesForFamilyName: family])
//        {
//            NSLog(@"  %@", name);
//        }
//    }
//
   
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    self.navigationItem.title = @"My Portfolio";
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:0.20 green:0.67 blue:0.86 alpha:1.0]];
    
    
    //my original array of stocks for testing purpose with hard coded values
//    arrayOfStocks = [NSArray arrayWithObjects:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      @"AAPL",@"name",
//      [NSNumber numberWithDouble:700.50],@"value",
//      [NSNumber numberWithDouble:+1.50],@"gainLoss",
//      nil],
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      @"FB",@"name",
//      [NSNumber numberWithDouble:+250.00],@"value",
//      [NSNumber numberWithDouble:+4.50],@"gainLoss",
//      nil],
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      @"GOOG",@"name",
//      [NSNumber numberWithDouble:+2500.50],@"value",
//      [NSNumber numberWithDouble:-3.50] ,@"gainLoss",
//      nil],
//     nil];
    

    //NSDictionary* array = [self detailsOfStockSymbol:@"YHOO"];
    
    //NSLog(@"here");
    

    //set the table view
    
    //myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    //myTableView.delegate = self;
    //myTableView.dataSource = self;
    //[self.view addSubview:myTableView];
    
    //create an add button at the nav bar; (for adding a new stock to track)
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewStock:)];
    self.navigationItem.rightBarButtonItem = addButton;
    
}


//this method returns details of a stock based on stockSymbol
-(NSDictionary*)detailsOfStockSymbol:(NSString*)stockLookupString {
    NSURL *requestUrl = [NSURL URLWithString:[NSString stringWithFormat:yahooLoadStockDetailsURLString, [stockLookupString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10
                                    ];
    
    [request setHTTPMethod: @"GET"];
    
    NSError *requestError = nil;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 =
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&urlResponse error:&requestError];
    
    NSString* dataResponse = [[NSString alloc]initWithData:response1 encoding:NSASCIIStringEncoding];
    
    
    NSString *stringWithoutSpaces = [dataResponse
                                     stringByReplacingOccurrencesOfString:@"/**/cbfunc(" withString:@""];
    
    NSString *newString = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-2];
    
    NSData* data = [newString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSError* error;
    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    return dictionary;
}


- (void)addNewStock:(id)sender{
    addNewStockController *add=[[addNewStockController alloc]init];
    [self.navigationController pushViewController:add animated:YES];
    
}

//set the number of rows in the tableView
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//   return arrayOfStocks.count;
//}



//My Original tableViewCEll
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    //use the custom table view cell
//    CustomTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//    if (!cell) {
//        cell = [[CustomTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    }
//    
//    
//    NSDictionary* infoDictionary = arrayOfStocks[indexPath.row];
//    
//    //display the stock name
//    
//    cell.stockLbl.text = [infoDictionary objectForKey:@"name"];
//    
//   //NSDictionary* array = [self detailsOfStockSymbol:cell.stockLbl.text];
//    
//    
//    //display the stock current value
//    
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
//    
//            NSDictionary* array = [self detailsOfStockSymbol:cell.stockLbl.text];
//    
//            dispatch_async(dispatch_get_main_queue(), ^{ // 2
//                //Update my UI
//                cell.valueLbl.text= [[[[array objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"quote"] objectForKey:@"Ask"];
//            });
//        });
//
//    
////    NSNumber *currentValue = [infoDictionary objectForKey:@"value"];
////    NSString *valueStr = [NSNumberFormatter localizedStringFromNumber:currentValue numberStyle:NSNumberFormatterCurrencyStyle];
////    cell.valueLbl.text = valueStr;
//    
//    
//    //display the gain/loss
//    
//    NSNumber *num1 = [infoDictionary objectForKey:@"gainLoss"];
//    NSString *numberStr = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterCurrencyStyle];
//    
//    cell.gainLossLbl.text = numberStr;
//    
//    //change color of the background; green for +, red for (-)
//    if([num1 doubleValue] < 0.0){
//        cell.gainLossLbl.backgroundColor = [UIColor redColor];
//    }
//    
//    else if([num1 doubleValue] > 0.0){
//       // NSLog(@"%f", [num1 doubleValue]);
//        cell.gainLossLbl.backgroundColor =  [UIColor colorWithRed:106/255.0f green:183/255.0f blue:9/255.0f alpha:1.0f];
//        NSString *plus = @"+";
//        cell.gainLossLbl.text = [plus stringByAppendingString:numberStr];
//    }
//    else{
//        cell.gainLossLbl.backgroundColor = [UIColor whiteColor];
//        cell.gainLossLbl.textColor = [UIColor blackColor];
//        
//    }
//    
//   
//    return cell;
//}

// initial method for tableView didSelectRowAtIndexPath
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    
//    NSDictionary* infoDictionary = arrayOfStocks[indexPath.row];
//    
//    StockDetailsViewController* info = [StockDetailsViewController new];
//    info.informationDictionary = infoDictionary;
//    [self.navigationController pushViewController:info animated:YES];
//    
//    
//}
//

-(void)viewWillAppear:(BOOL)animated{
    [self loadObjects];
    [self.tableView reloadData];
    [super viewDidAppear:animated];
}

//delete a stock from the tableview
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    return YES;
//}
//
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        PFObject *object = [self.objects objectAtIndex:indexPath.row];
//        [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//            
//            if (succeeded) {
//                NSLog(@"success in deleting stock");
//                [self loadObjects];
//            } else {
//                 NSLog(@"there was an error in deleting");
//            }
//        }];
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
