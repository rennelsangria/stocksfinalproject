//
//  CustomTableViewCell.m
//  StocksProject
//
//  Created by Rennel Sangria on 8/4/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "CustomTableViewCell.h"

@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //set the stock label
       //frame = CGRectMake(0, 0,
                                         //CGRectGetWidth(self.view.bounds),
                                         //CGRectGetHeight(self.view.bounds)); //Bes
        self.stockLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.frame.size.width/3, 20)];
        //self.stockLbl.backgroundColor = [UIColor greenColor];
        self.stockLbl.font = [UIFont fontWithName:@"Roboto-Light" size:20];
        [self addSubview:self.stockLbl];
        
        //set the gain/loss label
        self.valueLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/3, 10, self.frame.size.width/3, 20)];
        self.valueLbl.font = [UIFont fontWithName:@"Roboto-Light" size:16];
        //self.valueLbl.backgroundColor = [UIColor purpleColor];
        
        //self.valueLbl.textAlignment = NSTextAlignmentCenter;;
        //self.gainLossLbl.backgroundColor = [UIColor greenColor];
        [self addSubview:self.valueLbl];
        
        
        //set the gain/loss label
        self.gainLossLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width*2/3, 5, self.frame.size.width/3-10, 30)];
        self.gainLossLbl.textColor = [UIColor whiteColor];
        self.gainLossLbl.textAlignment = NSTextAlignmentCenter;
        self.gainLossLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
        
        [self addSubview:self.gainLossLbl];
        
        
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
