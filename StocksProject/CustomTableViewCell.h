//
//  CustomTableViewCell.h
//  StocksProject
//
//  Created by Rennel Sangria on 8/4/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (nonatomic, retain) UILabel* stockLbl;
@property (nonatomic, retain) UILabel* valueLbl;
@property (nonatomic, retain) UILabel* gainLossLbl;

@end
