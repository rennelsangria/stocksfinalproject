//
//  addNewStockController.m
//  StocksProject
//
//  Created by Rennel Sangria on 8/5/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "addNewStockController.h"

@interface addNewStockController ()

@end

@implementation addNewStockController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Pick a Stock";
    
    // Search bar
    search = [[UISearchBar alloc] init];
    [search setTintColor:[UIColor colorWithRed:233.0/255.0
                                         green:233.0/255.0
                                          blue:233.0/255.0
                                         alpha:1.0]];
    search.frame = CGRectMake(0, 0, self.view.frame.size.width ,41);
    search.delegate = self;
    search.showsBookmarkButton = NO;
    search.placeholder = @"Enter Stock Name or Symbol";
    [self.view addSubview:search];
    //[search release];
    
    stockTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, search.frame.origin.y + search.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - search.frame.size.height) style:UITableViewStylePlain];
    stockTableView.delegate = self;
    stockTableView.dataSource = self;
    [self.view addSubview:stockTableView];
    
    stocksList = [self arrayOfStockSymbols:searchText];
    
}


-(NSArray*)arrayOfStockSymbols:(NSString*)stockLookupString {
    NSURL *requestUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=%@&callback=YAHOO.Finance.SymbolSuggest.ssCallback", [stockLookupString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10
                                    ];
    
    [request setHTTPMethod: @"GET"];
    
    NSError *requestError = nil;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 =
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&urlResponse error:&requestError];
    
    NSString* dataResponse = [[NSString alloc]initWithData:response1 encoding:NSASCIIStringEncoding];
    
    
    NSString *stringWithoutSpaces = [dataResponse
                                     stringByReplacingOccurrencesOfString:@"YAHOO.Finance.SymbolSuggest.ssCallback(" withString:@""];
    
    NSString *newString = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-1];
    
    NSData* data = [newString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSError* error;
    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    return [[dictionary objectForKey:@"ResultSet"] objectForKey:@"Result"];
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:searchText{
   // NSLog(@"%@", searchText);
    stocksList = [self arrayOfStockSymbols:searchText];
    //NSLog(@"%@",stocksList);
    NSUInteger size = [stocksList count];
    NSLog(@"count of stock list: %lu", (unsigned long)size);
    
    [stockTableView reloadData];
}


//-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
//    search.frame = CGRectMake(0,MAX(0,scrollView.contentOffset.y),320,44);
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [stocksList count];
}



- (UITableViewCell *)tableView:myTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    customSearchTableViewCell* cell=[myTableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell){
        cell = [[customSearchTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* infoDictionary = stocksList[indexPath.row];
    
    cell.stockSymbolLbl.text = [infoDictionary objectForKey:@"symbol"];
    cell.exchangeLbl.text = [infoDictionary objectForKey:@"exchDisp"];
    cell.stockNameLbl.text = [infoDictionary objectForKey:@"name"];

    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    StockDetailsViewController* sdvc = [StockDetailsViewController new];
//    sdvc.theStockObject = [self.objects objectAtIndex:indexPath.row];
//    [self.navigationController pushViewController:sdvc animated:YES];
//    NSLog(@"%@", sdvc.theStockObject);
    
    NSDictionary* infoDictionary = stocksList[indexPath.row];
    
    StockDetailsViewController* sdvc = [StockDetailsViewController new];
    sdvc.informationDictionary = infoDictionary;
    [self.navigationController pushViewController:sdvc animated:YES];
    NSLog(@"The object passed via  addNewStockController: %@", sdvc.informationDictionary);
}




/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
