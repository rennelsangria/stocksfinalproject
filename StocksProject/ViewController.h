//
//  ViewController.h
//  StocksProject
//
//  Created by Rennel Sangria on 8/3/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import "StockDetailsViewController.h"
#import "CustomTableViewCell.h"
#import "addNewStockController.h"
#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>


@interface ViewController : PFQueryTableViewController
//UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* arrayOfStocks;
    UITableView* myTableView;
}



@end

