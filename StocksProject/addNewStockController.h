//
//  addNewStockController.h
//  StocksProject
//
//  Created by Rennel Sangria on 8/5/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "customSearchTableViewCell.h"
#import <UIKit/UIKit.h>
#import "StockDetailsViewController.h"
#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>

@interface addNewStockController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>{
     UISearchBar* search;
     UITableView* stockTableView;
     NSArray *stocksList;
     NSString *searchText;
    }

@property (nonatomic, strong) UISearchController *searchController;


@end
