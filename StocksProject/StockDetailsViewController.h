//
//  StockDetailsViewController.h
//  StocksProject
//
//  Created by Rennel Sangria on 8/3/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//
#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import <UIKit/UIKit.h>


@interface StockDetailsViewController : UIViewController<UITextFieldDelegate>{
    
    UITextField* initialPrice;
    UITextField* quantity;
    UITextField* fees;
    UILabel* nameLbl;
    UILabel* currentAskPriceLbl;
    UILabel* stockPrice;
    
}
//create a property of an object
@property(nonatomic, strong) NSDictionary* informationDictionary;
@property (strong,nonatomic) PFObject *theStockObject;
@end
