//
//  StockDetailsViewController.m
//  StocksProject
//
//  Created by Rennel Sangria on 8/3/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "StockDetailsViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface StockDetailsViewController ()

@end

@implementation StockDetailsViewController


static NSString *yahooLoadStockDetailsURLString = @"http://query.yahooapis.com/v1/public/yql?q=select%%20*%%20from%%20yahoo.finance.quotes%%20where%%20symbol%%20%%3D%%20%%22%@%%22&format=json&env=store%%3A%%2F%%2Fdatatables.org%%2Falltableswithkeys&callback=cbfunc";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self.navigationItem setTitle:@"Stock Details"];
    
    //set the stock symbol
  
    nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 30)];
    nameLbl.text = [self.theStockObject objectForKey:@"stockSymbol"];
    
    //is this a new object or an old object to modify?
    if (nameLbl.text == nil){
        NSLog(@"The object passed via  addNewStockController: %@", [self.informationDictionary objectForKey:@"symbol"]);
        nameLbl.text = [self.informationDictionary objectForKey:@"symbol"];
        
        //if new object, the use the Save button
        UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Save"
                                       style:UIBarButtonItemStylePlain
                                       target:self
                                       action:@selector(saveStock:)];
        
        [self.navigationItem setRightBarButtonItem: saveButton];

    }else{
        //if old, create an Update button
        UIBarButtonItem *updateButton = [[UIBarButtonItem alloc]
                                       initWithTitle:@"Update"
                                       style:UIBarButtonItemStylePlain
                                       target:self
                                       action:@selector(updateStock:)];
        
        [self.navigationItem setRightBarButtonItem: updateButton];
        
        //also, create a delete button at the bottom
        UIButton *deleteBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [deleteBtn addTarget:self
                   action:@selector(deleteStock:)
         forControlEvents:UIControlEventTouchUpInside];
        [deleteBtn setTitle:@"Delete Stock" forState:UIControlStateNormal];
       // deleteBtn.frame = CGRectMake(80.0, 210.0, 160.0, 40.0);
        float X_Co = (self.view.frame.size.width - 120)/2;
        [deleteBtn setFrame:CGRectMake(X_Co, 440, 120, 30)];
        deleteBtn.backgroundColor = [UIColor redColor];
        [deleteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //CGRect bounds = deleteBtn.superview.bounds;
        //deleteBtn.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
        [self.view addSubview:deleteBtn];

    }
    nameLbl.textColor = [UIColor blackColor];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    [nameLbl setFont: [nameLbl.font fontWithSize: 30]];
    [self.view addSubview:nameLbl];
    
    
    //display the current ask price of the stock
    
    currentAskPriceLbl = [[UILabel alloc]initWithFrame:CGRectMake(30, 80, self.view.frame.size.width/2, 30)];
    currentAskPriceLbl.text= @"Current Ask Price:";
    currentAskPriceLbl.textColor = [UIColor brownColor];
    currentAskPriceLbl.font = [UIFont fontWithName:@"Roboto-Light" size:18];
    [self.view addSubview:currentAskPriceLbl];
    
    
    //get current value
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{ // 1
        
        NSDictionary* array = [self detailsOfStockSymbol:nameLbl.text];
        
        dispatch_async(dispatch_get_main_queue(), ^{ // 2
            
            float currentValue = [[[[[array objectForKey:@"query"] objectForKey:@"results"] objectForKey:@"quote"] objectForKey:@"Ask"]floatValue];
           // NSLog(@"The current ask value is %f", currentValue);
            
            //display the current value
            stockPrice = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 + 40, 80, self.view.frame.size.width/2 - 40, 30)];
            stockPrice.text= [NSString stringWithFormat: @"$%.2f",currentValue];
            stockPrice.textColor = [UIColor blueColor];
            stockPrice.textAlignment = NSTextAlignmentCenter;
            stockPrice.font = [UIFont fontWithName:@"Roboto-Light" size:20];
            //stockPrice.backgroundColor = [UIColor grayColor];
            [self.view addSubview:stockPrice];
        });
    });

    // display the initial price
    
    UILabel* priceLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 150, self.view.frame.size.width, 30)];
    priceLbl.text =  @"Initial Price";
    priceLbl.textColor = [UIColor brownColor];
    priceLbl.textAlignment = NSTextAlignmentCenter;
    priceLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    [self.view addSubview:priceLbl];
    
    initialPrice = [[UITextField alloc] initWithFrame:CGRectMake(30, 180, self.view.frame.size.width-60, 40)];
    initialPrice.borderStyle = UITextBorderStyleRoundedRect;
    initialPrice.font = [UIFont systemFontOfSize:14];
    initialPrice.placeholder = @"Enter stock price";
    initialPrice.autocorrectionType = UITextAutocorrectionTypeNo;
    initialPrice.keyboardType = UIKeyboardTypeDefault;
    initialPrice.returnKeyType = UIReturnKeyDefault;
    initialPrice.clearButtonMode = UITextFieldViewModeWhileEditing;
    initialPrice.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    initialPrice.font = [UIFont fontWithName:@"Roboto-Light" size:16];
    initialPrice.delegate = self;
    
    NSDecimalNumber *initialStockPrice = [self.theStockObject objectForKey:@"initialPrice"];
   // NSLog(@"%@",initialStockPrice);
    //NSLog(@"Initial price is %@", initialStockPrice);
    initialPrice.text = [initialStockPrice stringValue];
    [self.view addSubview:initialPrice];
    
    //display the quantity
    
    UILabel* qtyLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 240, self.view.frame.size.width, 30)];
    qtyLbl.text =  @"Quantity";
    qtyLbl.textColor = [UIColor brownColor];
    qtyLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    qtyLbl.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:qtyLbl];
    
    quantity = [[UITextField alloc] initWithFrame:CGRectMake(30, 270, self.view.frame.size.width-60, 40)];
    quantity.borderStyle = UITextBorderStyleRoundedRect;
    quantity.font = [UIFont systemFontOfSize:14];
    quantity.placeholder = @"Enter stock quantity";
    quantity.autocorrectionType = UITextAutocorrectionTypeNo;
    quantity.keyboardType = UIKeyboardTypeDefault;
    quantity.returnKeyType = UIReturnKeyDefault;
    quantity.clearButtonMode = UITextFieldViewModeWhileEditing;
    quantity.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    quantity.font = [UIFont fontWithName:@"Roboto-Light" size:16];
    quantity.delegate = self;
    NSNumber *numShares = [self.theStockObject objectForKey:@"numberOfShares"];
    quantity.text = [numShares stringValue];
    [self.view addSubview:quantity];
    
    //display the fees
    
    UILabel* feesLbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 330, self.view.frame.size.width, 30)];
    feesLbl.text =  @"Fees";
    feesLbl.textColor = [UIColor brownColor];
    feesLbl.textAlignment = NSTextAlignmentCenter;
    feesLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    [self.view addSubview:feesLbl];
    
    fees = [[UITextField alloc] initWithFrame:CGRectMake(30, 360, self.view.frame.size.width-60, 40)];
    fees.borderStyle = UITextBorderStyleRoundedRect;
    fees.font = [UIFont systemFontOfSize:15];
    fees.placeholder = @"Enter the buy + sell fees";
    fees.autocorrectionType = UITextAutocorrectionTypeNo;
    fees.keyboardType = UIKeyboardTypeDefault;
    fees.returnKeyType = UIReturnKeyDefault;
    fees.clearButtonMode = UITextFieldViewModeWhileEditing;
    fees.font = [UIFont fontWithName:@"Roboto-Light" size:16];
    fees.delegate = self;
    fees.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    NSNumber *feePrice = [self.theStockObject objectForKey:@"fees"];
    fees.text = [feePrice stringValue];
    [self.view addSubview:fees];
    
}


//the next two methods will hide the keyboard when RETURN or DONE button is touched
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


//save new stock object to parse.com Stock class
-(void)saveStock:(id)sender{
    
    //using REST HTTP POST REQUEST
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://parse.com/1/classes/Stock"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"IBjNAePzVd3jTPVpfArAah4n2bPBsMR6ZDFow9Dx" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:@"X9Q4NvAmffyQDVovDHuWNnO78iWl7RaQMAaqqCiq" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    
    //convert the initialPrice from string to number
    NSNumberFormatter *p = [[NSNumberFormatter alloc]init];
    p.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *initPrice = [p numberFromString:initialPrice.text];
    
    //convert the quantity from string to number
    NSNumberFormatter *s = [[NSNumberFormatter alloc]init];
    s.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *numShares = [s numberFromString:quantity.text];
    
    //convert the fees from string to number
    NSNumberFormatter *f = [[NSNumberFormatter alloc]init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *feePrice = [f numberFromString:fees.text];
    
    //create a dictionary that contains the columns of the parse Stock class
    NSDictionary* diction = @{@"stockSymbol":nameLbl.text,
                              @"initialPrice":initPrice,
                              @"numberOfShares":numShares,
                              @"fees":feePrice};
    
    NSError* error;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:diction
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
    [request setHTTPBody:dataFromDict];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);
    [self.navigationController popViewControllerAnimated:YES];
}


//PARSE update function
//this method updates an existing object in parse
-(void)updateStock:(id)sender{
    
    //retrieve the objectId
    
    NSLog(@"Object ID to update: %@", [self.theStockObject objectId]);
    
    PFQuery *query = [PFQuery queryWithClassName:@"Stock"];
    [query getObjectInBackgroundWithId:[self.theStockObject objectId] block:^(PFObject *stockObj, NSError * error){
        
        //update the initial price
        //convert the initialPrice from string to number
        NSNumberFormatter *p = [[NSNumberFormatter alloc]init];
        p.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *newInitPrice = [p numberFromString:initialPrice.text];
        
        stockObj[@"initialPrice"] = newInitPrice;  //set the updated price
        
        
        //update the quantity
        //convert the qty from string to number
        NSNumberFormatter *s = [[NSNumberFormatter alloc]init];
        s.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *newQty = [p numberFromString:quantity.text];
        
        stockObj[@"numberOfShares"] = newQty;  //set the updated qty
        
        //update the fees
        //convert the fees from string to number
        NSNumberFormatter *f = [[NSNumberFormatter alloc]init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *newFeePrice = [p numberFromString:fees.text];
        
        stockObj[@"fees"] = newFeePrice;  //set the updated fee price
        
        [stockObj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
            if(succeeded){
                NSLog(@"Updated object: %@", [self.theStockObject objectId]);
            }else{
                 NSLog(@"There was an error");
            }
            
        }];
        
    }];
    
    [self.navigationController popViewControllerAnimated:YES];
}


//delete function

-(void)deleteStock:(id)sender{
    
    //retrieve the objectId
    
    NSLog(@"Object ID to update: %@", [self.theStockObject objectId]);
    
    PFQuery *query = [PFQuery queryWithClassName:@"Stock"];
    [query getObjectInBackgroundWithId:[self.theStockObject objectId] block:^(PFObject *stockObj, NSError * error){
        
        [stockObj deleteInBackground];
    }];
    
    [self.navigationController popViewControllerAnimated:YES];
}


//DetailsOfStockSymbol function
  //this method returns details of a stock based on stockSymbol
-(NSDictionary*)detailsOfStockSymbol:(NSString*)stockLookupString {
    NSURL *requestUrl = [NSURL URLWithString:[NSString stringWithFormat:yahooLoadStockDetailsURLString, [stockLookupString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:requestUrl
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10
                                    ];
    
    [request setHTTPMethod: @"GET"];
    
    NSError *requestError = nil;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 =
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&urlResponse error:&requestError];
    
    NSString* dataResponse = [[NSString alloc]initWithData:response1 encoding:NSASCIIStringEncoding];
    
    
    NSString *stringWithoutSpaces = [dataResponse
                                     stringByReplacingOccurrencesOfString:@"/**/cbfunc(" withString:@""];
    
    NSString *newString = [stringWithoutSpaces substringToIndex:[stringWithoutSpaces length]-2];
    
    NSData* data = [newString dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSError* error;
    NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    return dictionary;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
