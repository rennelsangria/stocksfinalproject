//
//  customSearchTableViewCell.m
//  StocksProject
//
//  Created by Rennel Sangria on 8/11/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "customSearchTableViewCell.h"

@implementation customSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //set the stock label
        //self.backgroundColor = [UIColor lightGrayColor];
        self.stockSymbolLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, self.frame.size.width/3, 20)];
        self.stockSymbolLbl.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
        [self addSubview:self.stockSymbolLbl];
        
        //set the exchange label
        self.exchangeLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width/3, 10, self.frame.size.width/3-20, 20)];
        self.exchangeLbl.textColor = [UIColor grayColor];
        self.exchangeLbl.font = [UIFont fontWithName:@"Roboto-Light" size:12];
        [self addSubview:self.exchangeLbl];
        
        
        //set the stock name label
        self.stockNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width*2/3-20, 10, self.frame.size.width/3-10, 20)];
        self.stockNameLbl.textColor = [UIColor brownColor];
        self.stockNameLbl.font = [UIFont fontWithName:@"Roboto-Light" size:12];
        //self.stockNameLbl.backgroundColor = [UIColor lightGrayColor];
        //self.stockNameLbl.textAlignment = NSTextAlignmentCenter;;
        [self addSubview:self.stockNameLbl];

        
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
